#!/usr/bin/python3

import os
import sys


rootdir = sys.argv[1]

exclude_list = sys.argv[2:]


def get_files(rootdir: str,
              extension: str,
              exclude_list: list) -> list:
    project_files = list()
    for folder, subs, files in os.walk(rootdir):
        for filename in files:
            if filename.endswith(extension):
                filepath = os.path.join(folder, filename)

                excluder_found = False
                for excluder in exclude_list:
                    if excluder in filepath:
                        excluder_found = True
                        break
                if excluder_found:
                    continue

                project_files.append('  "{0}",'.format(filepath))

    return project_files


project_content = []
project_content.append('{')
project_content.append(' "files":')
project_content.append(' [')

project_content.extend(get_files(rootdir, ".md", exclude_list))
project_content.extend(get_files(rootdir, ".py", exclude_list))
project_content.extend(get_files(rootdir, ".ui", exclude_list))
project_content.extend(get_files(rootdir, ".qml", exclude_list))
project_content.extend(get_files(rootdir, ".sh", exclude_list))
project_content.extend(get_files(rootdir, ".json", exclude_list))
project_content.extend(get_files(rootdir, ".yml", exclude_list))
project_content.extend(get_files(rootdir, "Dockerfile", exclude_list))
project_content.extend(get_files(rootdir, ".sql", exclude_list))
project_content.extend(get_files(rootdir, ".csv", exclude_list))

project_content[-1] = project_content[-1].replace(",", "")
project_content.append(' ]')
project_content.append('}')

project_content = '\n'.join(project_content)
print("Project content: {0}".format(project_content))

project_name = os.path.basename(os.path.normpath(os.path.abspath(rootdir)))
project_filename = os.path.join(rootdir, "{0}.pyproject".format(project_name))

f = open(project_filename, "w")
f.write(project_content)
f.close()
